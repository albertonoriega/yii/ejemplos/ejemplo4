<?php

/* @var $this yii\web\View */

$this->title = 'Ejemplo 4 Yii';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestion de alumnos</h1>

        <p class="lead">Aplicación que nos permite gestionar un conjunto de alumnos</p>

    
    </div>


</div>

<div class="body-content">

        <div class="row">
            <div>
            Creamos el CRUD de la base de datos de alumnos

            <ul>
                 <li>Importamos la base de datos a PHPmyadmin</li>
                 <li>Creamos los modelos y el CRUD para latabla alumnos de la BBDD con gii</li>
                 <li>Cambiamos los label de todos los botones para ponerlos en castellano</li>
                    <ul>
                        <li>Vista index: Crear departamento</li>
                        <li>Vista view: Actualizar, Borrar, y cambiar el mensaje de confirmar borrado del registro</li>
                        <li>Vista update: Aceptar </li>
                    </ul>
                <li>Cambiamos el menu y el footer del layout</li>
            </ul>

        
            </div>

        </div>
    </div>
